import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the ActorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-actor',
  templateUrl: 'actor.html',
})
export class ActorPage {

  actor: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController 
  ) {
    this.actor = ""; 
  }

  ionViewDidLoad() {
    this.actor = this.navParams.get('act');
  }
  public closeActor(){
    this.viewCtrl.dismiss();
}
}
