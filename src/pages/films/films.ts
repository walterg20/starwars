import { Component } from "@angular/core";
import { NavController, NavParams, ModalController } from "ionic-angular";
import { FilmsServiceProvider } from "../../providers/films-service/films-service";
import { ActoresServiceProvider } from "../../providers/actores-service/actores-service";
import { UturuncoUtils } from "../../utils/uturuncoUtils";
import { Film } from "../../models/film";
/**
 * Generated class for the FilmsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@Component({
  selector: "page-films",
  templateUrl: "films.html"
})
export class FilmsPage {
  filmes: Film[] = [];
  actores: any[] = [];
  
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public wsdlFilms: FilmsServiceProvider,
    public wsdlActor: ActoresServiceProvider,
    public modalCtrl: ModalController
  ) {}

  ionViewDidLoad() {
    //console.log(JSON.stringify(UturuncoUtils.arrayFilms[0]));
    this.filmes = UturuncoUtils.arrayFilms;
  }
  /*
  async getFilms() {
    const data = await this.wsdlFilms.getFilms().then();
    if (data) {
      this.filmes = JSON.parse(JSON.stringify(data["results"]));
      for (let x = 0; x < this.filmes.length; x++) {
        const actores = [];
        for (let i = 0; i < this.filmes[x].characters.length; i++) {
          //          actores.push(this.getActor(this.filmes[x].characters[i]));
          this.filmes[x].characters[i] = this.getActor(
            this.filmes[x].characters[i]
          );
        }
        //      this.filmes[x].characters = actores;
      }
    }
  }
  async getActor(url) {
    const actor = await this.wsdlActor.getActor(url).then();
    if (actor) {
      return actor;
    }
  }
  */

 public openActor(act){ 
  var modalPage = 
  this.modalCtrl.create('ActorPage', { act: act }); 
  modalPage.present(); }
}
