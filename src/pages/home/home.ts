import { Component } from "@angular/core";
import { NavController } from "ionic-angular";
import { FilmsPage } from "../films/films";
import { FilmsServiceProvider } from "../../providers/films-service/films-service";
import { ActoresServiceProvider } from "../../providers/actores-service/actores-service";
import { UturuncoUtils } from "../../utils/uturuncoUtils";
import { Film } from "../../models/film";

@Component({
  selector: "page-home",
  templateUrl: "home.html"
})
export class HomePage {
  actores = [];
  prosess: boolean;
  constructor(
    public wsdlFilms: FilmsServiceProvider,
    public wsdlActor: ActoresServiceProvider,
    public navCtrl: NavController
  ) {}
  ionViewDidLoad() {
    this.getFilms();
  }

  async verFilms() {
    this.navCtrl.push(FilmsPage);
  }
  async getFilms() {
    this.prosess = true;
    const data = await this.wsdlFilms
      .getFilms()
      .then()
      .catch(err => {
        console.log(err);
      });
    if (data) {
      const filmData = JSON.parse(JSON.stringify(data["results"]));
      UturuncoUtils.arrayFilms = [];
      for (let x = 0; x < filmData.length; x++) {
        this.actores = [];
        for (let i = 0; i < filmData[x].characters.length; i++) {
          const actor = await this.wsdlActor
            .getActor(filmData[x].characters[i])
            .then();
          if (actor) {
            this.actores.push(actor);
          }
        }
        let film = new Film();
        film.actores = this.actores;
        film.film = filmData[x];
        UturuncoUtils.arrayFilms.push(film);
      }
      this.prosess = false;
    }
  }
}
