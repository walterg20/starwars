import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { UturuncoUtils } from "../../utils/uturuncoUtils";

@Injectable()
export class FilmsServiceProvider {
  other_header;
  api;

  constructor(public http: HttpClient) {
    const header = new HttpHeaders();
    this.other_header = header.append("Accept", "application/json");
    this.other_header = header.append(
      "Content-Type",
      "application/x-www-form-urlencoded"
    );

    this.api = UturuncoUtils.URL + "films/";
  }
  getFilms() {
    return this.http.get(this.api, { headers: this.other_header }).toPromise();
  }
}
