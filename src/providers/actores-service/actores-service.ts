import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { UturuncoUtils } from "../../utils/uturuncoUtils";

/*
  Generated class for the ActoresServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ActoresServiceProvider {
  other_header;
  api;

  constructor(public http: HttpClient) {
    const header = new HttpHeaders();
    this.other_header = header.append("Accept", "application/json");
    this.other_header = header.append(
      "Content-Type",
      "application/x-www-form-urlencoded"
    );

    this.api = UturuncoUtils.URL + "peoples/";
  }
  getActores() {
    return this.http.get(this.api, { headers: this.other_header }).toPromise();
  }
  getActor(id) {
    return this.http.get(id, { headers: this.other_header }).toPromise();
  }
}
