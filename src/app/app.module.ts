import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpClientModule } from '@angular/common/http';


import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { FilmsServiceProvider } from '../providers/films-service/films-service';
import { ActoresServiceProvider } from '../providers/actores-service/actores-service';
import { FilmsPage } from '../pages/films/films';
import { ActorPage } from '../pages/actor/actor';
import { ActorPageModule } from '../pages/actor/actor.module';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    FilmsPage,
    //ActorPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    ActorPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    FilmsPage,
    ActorPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    FilmsServiceProvider,
    ActoresServiceProvider
  ]
})
export class AppModule {}
